#include <cstdlib>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include "factory.hpp"

using namespace std;

namespace Canonical
{

    class Service
    {
        shared_ptr<LoggerCreator> creator_;

    public:
        Service(shared_ptr<LoggerCreator> creator)
            : creator_(creator)
        {
        }

        Service(const Service&) = delete;
        Service& operator=(const Service&) = delete;

        void use()
        {
            unique_ptr<Logger> logger = creator_->create_logger();
            logger->log("Client::use() has been started...");
            run();
            logger->log("Client::use() has finished...");
        }

    protected:
        virtual void run()
        {
        }
    };

    using LoggerFactory = std::unordered_map<std::string, shared_ptr<LoggerCreator>>;
}

using LoggerCreator = std::function<std::unique_ptr<Logger>()>;

class Service
{
    LoggerCreator logger_creator_;

public:
    Service(LoggerCreator logger_creator)
        : logger_creator_(logger_creator)
    {
    }

    Service(const Service&) = delete;
    Service& operator=(const Service&) = delete;

    void use()
    {
        unique_ptr<Logger> logger = logger_creator_();
        logger->log("Client::use() has been started...");
        run();
        logger->log("Client::use() has finished...");
    }

protected:
    virtual void run()
    {
    }
};

using LoggerFactory = std::unordered_map<std::string, LoggerCreator>;

int main()
{
    using namespace std;

    LoggerFactory logger_factory;
    logger_factory.insert(make_pair("ConsoleLogger", [] { return std::make_unique<ConsoleLogger>(); }));
    logger_factory.insert(make_pair("FileLogger", [] { return std::make_unique<FileLogger>("data.log"); }));
    logger_factory.insert(make_pair("DbLogger", &std::make_unique<DbLogger>));

    Service srv(logger_factory.at("DbLogger"));
    srv.use();

    vector<int> data = { 1, 2, 3 };

    for(const auto& item : data)
    {
        cout << item << endl;
    }

    for(auto it = data.begin(); it != data.end(); ++it)
    {
        const auto& item = *it;

        cout << item << endl;
    }
}
