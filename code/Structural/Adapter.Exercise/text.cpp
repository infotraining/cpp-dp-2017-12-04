#include "text.hpp"
#include "shape_factories.hpp"
#include <memory>

namespace
{
    using namespace  Drawing;

    bool is_registered =
            SingletonShapeFactory::instance()
                .register_creator(Text::id, &std::make_unique<Text>);

}
