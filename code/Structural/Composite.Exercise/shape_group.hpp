#ifndef SHAPEGROUP_HPP
#define SHAPEGROUP_HPP

#include <memory>
#include <vector>

#include "shape.hpp"

namespace Drawing
{
    using ShapePtr = std::unique_ptr<Shape>;

    class ShapeGroup : public CloneableShape<ShapeGroup>
    {
        std::vector<ShapePtr> shapes_;
    public:
        constexpr static auto id = "ShapeGroup";

        ShapeGroup() = default;

        ShapeGroup(const ShapeGroup& source)
        {
            for(const auto& shp : source.shapes_)
                shapes_.push_back(shp->clone());
        }

        ShapeGroup(ShapeGroup&& source) = default;

        void add(ShapePtr shp)
        {
            shapes_.push_back(std::move(shp));
        }

        ShapeGroup& operator=(const ShapeGroup& other)
        {
            ShapeGroup temp(other);
            swap(temp);
            return *this;
        }

        ShapeGroup& operator=(ShapeGroup&& other)
        {
            if (this != &other)
            {
                shapes_ = std::move(other.shapes_);
            }

            return *this;
        }

        void swap(ShapeGroup& other)
        {
            shapes_.swap(other.shapes_);
        }

        void draw() const override
        {
            for(const auto& shp : shapes_)
                shp->draw();
        }

        void move(int dx, int dy) override
        {
            for(const auto& shp : shapes_)
                shp->move(dx, dy);
        }
    };
}

#endif // SHAPEGROUP_HPP
