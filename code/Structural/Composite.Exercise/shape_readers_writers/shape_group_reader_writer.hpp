#ifndef SHAPEGROUPREADERWRITER_HPP
#define SHAPEGROUPREADERWRITER_HPP

#include "../shape_factories.hpp"
#include "../shape_group.hpp"
#include "shape_reader_writer.hpp"

namespace Drawing
{
    namespace IO
    {
        class ShapeGroupReaderWriter : public ShapeReaderWriter
        {
            ShapeFactory& shape_factory_;
            ShapeRWFactory& shape_rw_factory_;
        public:
            ShapeGroupReaderWriter(ShapeFactory& sf, ShapeRWFactory& srwf)
                : shape_factory_{sf}, shape_rw_factory_{srwf}
            {}

            void read(Shape &shp, std::istream &in) override
            {
                ShapeGroup& sg = static_cast<ShapeGroup&>(shp);

                int size;
                in >> size;

                for(int i = 0; i < size; ++i)
                {
                    std::string id;

                    in >> id;

                    auto shape = shape_factory_.create(id);
                    auto shape_rw = shape_rw_factory_
                            .create(make_type_index(*shape));
                    shape_rw->read(*shape, in);
                    sg.add(std::move(shape));
                }
            }

            void write(const Shape &shp, std::ostream &out) override
            {
                // TODO
            }
        };
    }
}

#endif // SHAPEGROUPREADERWRITER_HPP
