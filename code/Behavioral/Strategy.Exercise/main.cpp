#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>
#include <functional>

struct StatResult
{
    std::string description;
    double value;

    StatResult(const std::string& desc, double val) : description(desc), value(val)
    {
    }
};

using Data = std::vector<double>;
using Results = std::vector<StatResult>;

using Statistics = std::function<Results (const Data&)>;

struct Avg
{
    Results operator()(const Data& data)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);
        double avg = sum / data.size();

        return { {"AVG", avg} };
    }
};

struct MinMax
{
    Results operator()(const Data& data)
    {
        double min = *(std::min_element(data.begin(), data.end()));
        double max = *(std::max_element(data.begin(), data.end()));

        return {  {"MIN", min }, { "MAX", max } };
    }
};

struct Sum
{
    Results operator()(const Data& data)
    {
        double sum = std::accumulate(data.begin(), data.end(), 0.0);

        return { { "SUM", sum } };
    }
};

class StatGroup
{
    std::vector<Statistics> stats_;
public:
    Results operator ()(const Data& data)
    {
        Results group_results;
        for(const auto& s : stats_)
        {
            auto temp_result = s(data);
            std::move(temp_result.begin(), temp_result.end(), std::back_inserter(group_results));
        }

        return group_results;
    }

    void add(Statistics s)
    {
        stats_.push_back(s);
    }
};

class DataAnalyzer
{
    Statistics statistics_;
    Data data_;
    Results results_;

public:
    DataAnalyzer(Statistics stat) : statistics_{stat}
    {
    }

    void load_data(const std::string& file_name)
    {
        data_.clear();
        results_.clear();

        std::ifstream fin(file_name.c_str());
        if (!fin)
            throw std::runtime_error("File not opened");

        double d;
        while (fin >> d)
        {
            data_.push_back(d);
        }

        std::cout << "File " << file_name << " has been loaded...\n";
    }

    void set_statistics(Statistics stat)
    {
        statistics_ = stat;
    }

    void calculate()
    {
        auto stat_results = statistics_(data_);

        std::move(stat_results.begin(), stat_results.end(), std::back_inserter(results_));
    }

    const Results& results() const
    {
        return results_;
    }
};

void show_results(const Results& results)
{
    for (const auto& rslt : results)
        std::cout << rslt.description << " = " << rslt.value << std::endl;
}

int main()
{
    Avg avg;
    MinMax min_max;
    Sum sum;

    StatGroup std_stats;
    std_stats.add(avg);
    std_stats.add(min_max);
    std_stats.add(sum);

    DataAnalyzer da{std_stats};
    da.load_data("data.dat");
    da.calculate();   

    show_results(da.results());

    std::cout << "\n\n";

    da.load_data("new_data.dat");
    da.calculate();

    show_results(da.results());
}
