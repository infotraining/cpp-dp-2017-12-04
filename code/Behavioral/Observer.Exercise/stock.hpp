#ifndef STOCK_HPP_
#define STOCK_HPP_

#include <iostream>
#include <string>
#include <set>
#include <memory>

class Observer
{
public:
    virtual void update(std::string symbol, double price) = 0; // push model
    virtual ~Observer()
    {
    }
};

// Subject
class Stock
{
private:
    std::string symbol_;
    double price_;
    std::set<std::weak_ptr<Observer>, std::owner_less<std::weak_ptr<Observer>>> observers_;
public:
    Stock(const std::string& symbol, double price) : symbol_(symbol), price_(price)
    {
    }

    std::string get_symbol() const
    {
        return symbol_;
    }

    double get_price() const
    {
        return price_;
    }

    void attach(std::shared_ptr<Observer> o)
    {
        observers_.insert(o);
    }

    void detach(std::shared_ptr<Observer> o)
    {
        observers_.erase(o);
    }

    void set_price(double price)
    {
        if (price != price_)
        {
            price_ = price;
            notify();
        }
    }
protected:
    void notify()
    {
        auto it = observers_.begin();

        while(it != observers_.end())
        {
            std::shared_ptr<Observer> observer_alive = it->lock();
            if (observer_alive)
            {
                ++it;
                observer_alive->update(symbol_, price_);
            }
            else
            {
                it = observers_.erase(it);
            }
        }
    }
};

class Investor : public Observer
{
    std::string name_;

public:
    Investor(const std::string& name) : name_(name)
    {
    }

    void update(std::string symbol, double price) override
    {
        std::cout << name_ << " is notified: " << symbol << " - " << price << std::endl;
    }
};

#endif /*STOCK_HPP_*/
