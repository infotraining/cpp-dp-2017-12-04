#include "bank_account.hpp"

namespace Bank
{
    const BankAccount::NormalState BankAccount::normal;
    const BankAccount::OverdraftState BankAccount::overdraft;
}
